[Source](http://staffwww.fullcoll.edu/sedwards/Nano/UsefulNanoKeyCommands.html 
"Permalink to Introduction to the nano Text Editor")

# Introduction to the nano Text Editor

# Useful Nano Keyboard Commands

Since the **nano** editor doesn't really support the use of a mouse, 
here's a table of commands you're likely to find very useful. You 
should experiment with every one of them and try to memorize them, it 
can speed up your work tremendously. (To see a list of all **nano** 
commands, type **Ctrl+g** when in the editor.)

NOTE: There is no "undo" command in **nano**, so save your work often!

  
  

**Topics**

[File Management][1]

[Copy and Paste][2]

[Navigating Through Code][3]

[Search and Replace][4]

[Named Key Equivalents][5]

  
  

**File Management**

| ----- |
| 

Task

 | 

Keystroke

 |  
| 

Open a file from within **nano**

 | 

**Ctrl+r**

**NOTE:** tab completion is in effect; also, once this command has 
been entered, notice the new menu items at the bottom of the screen. 
For example, **Ctrl+T** will allow you to browse the file system and 
look for a file to open.

 |  
| 

Display the next file buffer

 | 

**Alt+>**

 |  
| 

Display the previous file buffer

 | 

**Alt+<**

 |  
| 

Save the current file buffer to disk

 | 

**Ctrl+o**

 |  
| 

Close the current file buffer

 | 

**Ctrl+x**

**NOTE:** If the file hasn't been saved yet, you'll be asked if you 
want to save it. Also, if there's only one file buffer open, closing 
it will exit from **nano**.

 | 

  
  

**Copy and Paste**

| ----- |
| 

Task

 | 

Keystroke

 |  
| 

Select a region for a cut or paste operation

 | 

**Alt+a**

**NOTE:** After setting a mark with Alt+a, move the cursor to define 
the region, you should see it highlighted as you move the cursor. 
Also, to cancel the definition of the region just enter Alt+a again.

 |  
| 

Copy a highlighted region into the clipboard

 | 

**Alt+^**

 |  
| 

Cut a highlighted region into the clipboard

 | 

**Ctrl+k**

 |  
| 

Paste the contents of the clipboard at the current cursor position

 | 

**Ctrl+u**

 |  
| 

Cut from the current cursor position to the end-of-line (EOL)

 | 

**Ctrl+k**

**NOTE:** This command doesn't require highlighting of the region.

 | 

  
  

**Navigating Through Code**

| ----- |
| 

Task

 | 

Keystroke

 |  
| 

Go to beginning of file

 | 

**Alt+**

 |  
| 

Go to end of file

 | 

**Alt+/**

 |  
| 

Move forward one screenful

 | 

**Ctrl+v**

 |  
| 

Move backward one screenful

 | 

**Ctrl+y**

 |  
| 

Go to a target line number

 | 

**Alt+g**

 |  
| 

Jump to matching open/close symbol

 | 

**Alt+]**

**NOTE: **Very useful for finding mismatched brace compiler errors!

 |  
| 

Window scrolling

 | 

**Alt+=** to scroll down, **Alt+-** to scroll up

 |  
| 

Indenting/Outdenting selected blocks

 | 

Use **Alt+a** to select a block, then **Alt+}** will indent the 
selected block, and **Alt+{** will outdent the block.

 | 

  
  

**Search and Replace**

| ----- |
| 

Task

 | 

Keystroke

 |  
| 

Search for a target string

 | 

**Ctrl+w**

**NOTE:** Once this command has been entered, notice the new menu 
items at the bottom of the screen, such as toggling the direction of 
the search (Alt+B) or replacing the search string with a different 
string (Ctrl+R)

 |  
| 

Repeat the last search

 | 

**Alt+w**

 |  
| 

Toggle direction for next search

 | 

**Ctrl+w** followed by **Ctrl+b**

 |  
| 

Search and replace

 | 

**Alt+r**

 | 

  
  

  
  

**Named Key Equivalents**

| ----- |
| 

Task

 | 

Keystroke

 |  
| 

Home

 | 

**Ctrl+a**

 |  
| 

End

 | 

**Ctrl+e**

 |  
| 

Page Up

 | 

**Ctrl+y**

 |  
| 

Page Down

 | 

**Ctrl+v**

 |  
| 

Arrow Keys

 | 

**Ctrl+f** (right)**, Ctrl+b** (left)**, Ctrl+n** (down)**, Ctrl+p** 
(up)

 |  
| 

Tab

 | 

**Ctrl+i**

 |  
| 

Backspace

 | 

**Ctrl+h**

 |  
| 

Delete

 | 

**Ctrl+d**

 |  
| 

Return

 | 

**Ctrl+m**

 | 

  
  

  
  

[1]: http://staffwww.fullcoll.edu#FileManagement
[2]: http://staffwww.fullcoll.edu#CopyAndPaste
[3]: http://staffwww.fullcoll.edu#NavigatingThroughCode
[4]: http://staffwww.fullcoll.edu#SearchAndReplace
[5]: http://staffwww.fullcoll.edu#NamedKeyEquivalents

  
